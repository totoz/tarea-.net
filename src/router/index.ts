import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Universities",
    component: () => import("../views/Universities/Universities.vue")
  },
  {
    path: "/Users",
    name: "Users",
    component: () => import("../views/Users.vue")
  },
  {
    path: "/university/:id/users",
    name: "User",
    component: () => import("../views/Universities/UniversityUsers.vue")
  },
  {
    path: "/Courses",
    name: "Courses",
    component: () => import("../views/Courses.vue")
  },
  {
    path: "/error",
    name: "Server Error",
    component: () => import("../views/Errors/ServerError.vue")
  },
  {
    path: "*",
    name: "Not Found",
    component: () => import("../views/Errors/NotFound.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
