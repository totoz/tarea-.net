import { Vue, Component } from "vue-property-decorator";
import { AxiosError } from "axios";

@Component({})
export class NotificationMixin extends Vue {
  public success(title: string, text: string) {
    this.$notify({
      group: "feedback",
      duration: 5000,
      title: title,
      text: text,
      type: "success"
    });
  }

  public error(title: string, text: string) {
    this.$notify({
      group: "feedback",
      duration: 5000,
      title: title,
      text: text,
      type: "error"
    });
  }

  public errorHandling(error: AxiosError) {
    if (error.response) {
      this.error("Error", error.response.data);
    } else {
      this.$router.push("error");
      this.error("Error", "Oops algo salio mal :(");
    }
  }
}
