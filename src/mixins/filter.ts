import { Vue, Component } from "vue-property-decorator";
import University from "@/models/university";
import User from "@/models/user";
import Course from "@/models/course";

@Component({})
export class FilterMixin extends Vue {
  public get search() {
    return this.$store.state.search;
  }
  public filterData(items: Array<User | University | Course>) {
    return items.filter(item => {
      return item.name.toLowerCase().includes(this.search.toLowerCase());
    });
  }
}
