import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    search: ""
  },
  mutations: {
    SET_SEARCH(state, value: string) {
      state.search = value;
    }
  },
  actions: {},
  modules: {}
});
