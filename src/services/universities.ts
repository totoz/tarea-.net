import axios from "axios";
import University from "../models/university";
import User from "../models/user";

export async function getUniversities(): Promise<University[]> {
  const response = await axios.get("http://127.0.0.1:5000/api/Universities");
  return response.data;
}

export async function updateUniversity(university: University): Promise<void> {
  await axios.put("http://127.0.0.1:5000/api/Universities", university);
}

export async function createUniversity(university: University): Promise<University> {
  const response = await axios.post("http://127.0.0.1:5000/api/Universities", university);
  return response.data;
}

export async function deleteUniversity(universityName: string): Promise<void> {
  await axios.delete("http://127.0.0.1:5000/api/Universities/" + universityName);
}

export async function getUniversityUsers(universityName: string): Promise<User[]> {
  const response = await axios.get(`http://127.0.0.1:5000/api/Universities/${universityName}/users`);
  return response.data;
}
