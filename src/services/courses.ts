import axios from "axios";
import Course from "../models/course";

export async function getCourses(): Promise<Course[]> {
  const response = await axios.get("http://127.0.0.1:5000/api/Courses");
  return response.data;
}

export async function createCourse(course: Course): Promise<Course> {
  const response = await axios.post("http://127.0.0.1:5000/api/Courses", course);
  return response.data;
}

export async function updateCourse(course: Course): Promise<void> {
  await axios.put("http://127.0.0.1:5000/api/Courses", course);
}

export async function deleteCourse(id: number): Promise<void> {
  await axios.delete("http://127.0.0.1:5000/api/Courses/" + id);
}
