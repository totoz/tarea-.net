import axios from "axios";
import User from "../models/user";

export async function getUsers(): Promise<User[]> {
  const response = await axios.get("http://127.0.0.1:5000/api/Users");
  return response.data;
}

export async function createUser(user: User): Promise<User> {
  const response = await axios.post("http://127.0.0.1:5000/api/Users", user);
  return response.data;
}

export async function updateUser(user: User): Promise<void> {
  await axios.put("http://127.0.0.1:5000/api/Users", user);
}

export async function deleteUser(id: number): Promise<void> {
  await axios.delete("http://127.0.0.1:5000/api/Users/" + id);
}
