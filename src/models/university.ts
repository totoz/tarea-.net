export default interface University {
  name: string;
  accessUrl: string;
}
