export default interface User {
  id?: number;
  name: string;
  university?: string;
}
